'use strict';

/**
 * Created by viniciusmontanheiro on 23/06/15.
 */

angular.module('app.header', [])
    .controller('HeaderController', [function () {
        this.titulo = 'Header';

        $('.main-nav ul').onePageNav({
            currentClass: 'active',
            changeHash: false,
            scrollSpeed: 900,
            scrollOffset: 0,
            scrollThreshold: 0.3,
            filter: ':not(.no-scroll)'
        });

    }]);

