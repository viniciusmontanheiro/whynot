'use strict';

/**
 * Created by viniciusmontanheiro on 23/06/15.
 */

angular.module('app.contato', [])
    .controller('ContatoController', [function () {
        this.titulo = 'Contato';

        //Validação do formulário de contato
        var form = $('.contact-form');
        form.submit(function () {
            var $this = $(this);
            $.post($(this).attr('action'), function(data) {
                $this.prev().text(data.message).fadeIn().delay(3000).fadeOut();
            },'json');
            return false;
        });
    }]);