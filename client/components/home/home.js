'use strict';
/**
 * Created by viniciusmontanheiro on 22/06/15.
 */

angular.module('app.home', [])
    .controller('HomeController', [function () {
        this.titulo = 'Home';

        //Propriedades do carousel
        $('#event-carousel, #twitter-feed, #sponsor-carousel ').carousel({
            interval: false
        });
    }]);