'use strict';

/**
 * Created by viniciusmontanheiro on 23/06/15.
 */

angular.module('app.footer', [])
    .controller('FooterController', ['$timeout',function ($timeout) {
        this.titulo = 'Footer';
        this.copyright = 'Copyright © 2015 - WhyNot - Todos os direitos reservados';

        this.goToTop = function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        };

    }]);