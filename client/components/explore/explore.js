'use strict';
/**
 * Created by viniciusmontanheiro on 23/06/15.
 */

angular.module('app.explore', [])
    .controller('ExploreController', [function () {
        this.titulo = 'Explorar/Produtos';

        //Countdown js
        $("#countdown").countdown({
                date: "10 july 2017 12:00:00",
                format: "on"
            },

            function() {
                // callback function
            });

    }]);