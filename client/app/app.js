'use strict';

/**
 * Aplicação MEAN Stack Real Time voltada para comércio virtual
 * @name WhyNot
 * @requires NodeJs, AngularJs, MongoDB, Bower, GruntJs
 * @since 21/06/2015
 * @author Vinícius Montanheiro
 * @copyright WhyNot
 * @version 1.0.0
 * @licence MIT
 */

(function() {

    /**
     * Dependências
     */
    angular.module('whynot',
        ['ngNewRouter'
            ,'ngAnimate'
            ,'ngAria'
            ,'ngMaterial'
            ,'app.header'
            ,'app.home'
            ,'app.explore'
            ,'app.eventos'
            ,'app.about'
            ,'app.footer'
            ,'app.contato'
            ,'app.midia'
            ,'app.social'
        ])
        .controller('AppController', AppController);

    AppController.$inject = [
        '$router'
    ];

    //Rotas
    AppController.$routeConfig = [{
        path: '/',
        components: {
            'header':'header'
            ,'home':'home'
            ,'explore':'explore'
            ,'eventos' : 'eventos'
            ,'about' : 'about'
            ,'footer' : 'footer'
            ,'contato' : 'contato'
            ,'midia' : 'midia'
            ,'social':'social'
        }
    }];

    //Main controller
    function AppController($router) {

        //Scroll Menu
        function menuToggle() {
            var windowWidth = $(window).width();

            if(windowWidth > 767 ){
                $(window).on('scroll', function(){
                    if( $(window).scrollTop() > 150 ){
                        $('.main-nav').addClass('fixed-menu animated slideInDown');
                        $('.scrollToTop').fadeIn(600);
                    } else {
                        $('.main-nav').removeClass('fixed-menu animated slideInDown');
                        $('.scrollToTop').fadeOut(600);
                    }
                });
            }else{
                $('.main-nav').addClass('fixed-menu animated slideInDown');
            }
        }

        menuToggle();

        $(window).resize(function() {
            menuToggle();
        });
    }
})();


