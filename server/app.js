/**
 * Created by vinicius on 07/04/15.
 */

var childProcess = require('child_process');
var configs = require('./configs');
var Cluster = require('./settings/server/cluster.js');
var Server = require('./settings/server/server.js');

require('./settings/globals/variables.js');


var terminal = childProcess.exec;
//var command = "grunt mongo";

//terminal(command, function(err, stdout, stderr) {
//    if (err) {
//        console.error(new Error(':-( Falha ao iniciar mongo pelo grunt!'), err);
//    }

//Se, inicia aplicação com clusters
if (configs.clustered) {
    var clustering = new Cluster();
    clustering.init();
} else {
    Server();
}
//});

