'use strict';

var fs = require("fs");

/**
 * @description Utilidades
 * @constructor
 */
function Util() {
    /**
     * @description Cria um novo arquivo para escrita
     * @param URL
     * @param FLAGS
     * @returns {*}
     */
    this.fnCreateFile = function fnCreateFile(URL, FLAG) {
        return fs.createWriteStream(URL, {
            encoding: 'utf8',
            flags: FLAG
        });
    };

    /**
     * @description Abre arquivo para escrita
     * @param URL
     * @param FLAGS
     * @param CONTENT (conteúdo)
     * @returns {*}
     */
    this.fnWriteFile = function fnWriteFile(URL, FLAGS, CONTENT) {
        return fs.Writer({
            path: URL
            , mode: 755
            , flags: FLAGS
        })
            .write(CONTENT)
            .end()
    };
};

module.exports = Util;





