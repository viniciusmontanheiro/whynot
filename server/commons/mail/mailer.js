/**
 * Created by vinicius on 06/04/15.
 */

var nodemailer = require('nodemailer');

function Mail() {

    this.send = function (headers) {
        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'mrvini@live.com ',
                pass: ''
            }
        });

        var mailOptions = {
            from: headers.from,
            to: headers.to,
            subject: headers.subject,
            html: headers.html
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Message sent: ' + info.response);
            }
        });
    }

};
module.exports = Mail;

