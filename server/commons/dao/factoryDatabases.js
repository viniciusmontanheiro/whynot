'use strict';

/**
 * Created by vinicius on 01/04/15.
 */

var Mongo = require('../../settings/database/mongoConnector.js');
var Postgres = require('../../settings/database/postgresConnector.js');

function factoryDatabases() {

    this.connections = [];
    this.connectionsList = [
        {id: 'mongo', connector: Mongo.connector},
        {id: 'postgres', connector: Postgres.connector}
    ];

    this.getConnection = function (configs) {
        for (var i = 0; i < configs.length; i++) {
            for (var j = 0; j < this.connectionsList.length; j++) {
                if (this.connectionsList[j].id == configs[i].id) {
                    this.connections.push(this.connectionsList[j].connector(configs[i]));
                }
            }
        }
        return this;
    };

    this.findAll = function (Model, req, res, afterSearch) {
        for (var i in this.connections) {
            this.connections[i].dao.findAll(Model, req, res, afterSearch);
        }
    };

    this.save = function (Model, req, res, required, afterSave) {
        for (var i in this.connections) {
            this.connections[i].dao.save(Model, req, res, required, afterSave);
        }
    };

    this.update = function (Model, req, res, afterUpdate) {
        for (var i in this.connections) {
            this.connections[i].dao.update(Model, req, res, afterUpdate);
        }
    };

    this.delete = function (Model, req, res, afterDelete) {
        for (var i in this.connections) {
            this.connections[i].dao.delete(Model, req, res, afterDelete);
        }
    };

};

module.exports = factoryDatabases;



