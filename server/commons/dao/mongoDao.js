'use strict';

var Logger = global.logger;
var Validate = global.validation;
var DmMessage = Logger.getDmMessage();

function MongoDao() {

    //GET
    this.findAll = function find(Model, req, res, afterSearch) {

        Model.find(function (err, data) {

            if (err) {
                throw Logger.messageProcess(res, DmMessage.HANDLEERROR);
            }

            DmMessage.GETTED.DATA = data;
            Logger.messageProcess(res, DmMessage.GETTED);

            if (afterSearch) {
                afterSearch();
            }
        });

    };

    //POST
    this.save = function (Model, req,res,required, afterSave) {
        var body = req.body;

        if (body && required.length >= 1) {
            if (Validate.isEmpty(body, required)) {
                throw Logger.messageProcess(res, DmMessage.REQUIRED);
            }
        }

        Model.create(body, function (err, data) {
            if (err) {
                throw Logger.messageProcess(res, DmMessage.CREATEERROR);
            }
            Util.message.CREATED.DATA = data;
            Logger.messageProcess(res, DmMessage.CREATED);

            if(afterSave){
                afterSave();
            }

        });
    };

    // UPDATE
    this.update = function (Model, req,res, afterUpdate) {
        var item = req.body;
        var id = item._id;
        delete item._id;

        if (!id || id == undefined) {
            DmMessage.REQUIRED.ADDITIONAL = "IT NEEDS AN ID TO DELETE";
            return Logger.messageProcess(res, DmMessage.REQUIRED);
        }

        Model.update({_id: id}, item, {upsert: true}, function (err) {
            if (err) {
                throw Logger.messageProcess(res, DmMessage.CHANGEERROR);
            }
            Logger.messageProcess(res, DmMessage.CHANGED);

            if(afterUpdate){
                afterUpdate();
            }
        });
    };

    // DELETE
    this.delete = function (Model, req,res, afterDelete) {
        var id = req.body._id;

        if (!id || id == undefined) {
            DmMessage.REQUIRED.ADDITIONAL = "IT NEEDS AN ID TO DELETE";
            throw Logger.messageProcess(res,DmMessage.REQUIRED);
        }
        Model.find({ _id: id }).remove(function (err) {
            if (err) {
                throw Logger.messageProcess(res, DmMessage.REMOVEERROR);
            }
            throw Logger.messageProcess(res, DmMessage.REMOVED);

            if(afterDelete){
                afterDelete();
            }
        });
    };

};


module.exports = MongoDao;