'use strict';

var Logger = global.logger;
var Validate = global.validates;
var DmMessage = Logger.getDmMessage();

function PostgresDao(client){

    this.findAll  = function(req,res,afterSearch){
        client.query('SELECT * from vs_user', function(err, data) {
            if (err) {
                throw Logger.messageProcess(res, DmMessage.HANDLEERROR);
            }
            DmMessage.GETTED.DATA = data;

            Logger.messageProcess(res, DmMessage.GETTED);
            if (afterSearch) {
                afterSearch();
            }
        });

    }

};
module.exports = PostgresDao;
