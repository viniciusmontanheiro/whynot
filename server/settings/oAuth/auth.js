module.exports = {

    'facebookAuth': {
        'clientID': '384283948413217',
        'clientSecret': '130bc83847b32a6620d32d6ae692e42f',
        'callbackURL': 'http://localhost:8080/profile'
    },

    'twitterAuth': {
        'consumerKey': 'your-consumer-key-here',
        'consumerSecret': 'your-client-secret-here',
        'callbackURL': 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth': {
        'clientID': 'your-secret-clientID-here',
        'clientSecret': 'your-client-secret-here',
        'callbackURL': 'http://localhost:8080/auth/google/callback'
    }

};
