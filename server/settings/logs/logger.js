'use strict';

var log4js = require('log4js');

/**
 * @description Configurações do sistema de log
 */
log4js.configure({
    "appenders": [
        {
            type: "console"
        },

        {
            "type": "dateFile",
            "filename": __dirname + "/access.log",
            "pattern": "-yyyy-MM-dd",
            "category": "ALL"
        },

        {
            "type": "file",
            "filename": __dirname + "/app.log",
            "maxLogSize": 10485760,
            "numBackups": 3
        },

        {
            "type": "logLevelFilter",
            "level": "ERROR",
            "appender": {
                "type": "file",
                "filename": __dirname + "/errors.log"
            }
        }
    ],
    replaceConsole: true
});

/**
 * Seta log como modo geral
 * @type {Logger}
 */
var log = log4js.getLogger("ALL");

/**
 * @description Gerênciador de logs
 */
function LogsManager() {

    /**
     * @description Disponibilizando o status para o programador
     * @type {{}}
     */
    this.status = {};

    /**
     * @desc Processa log de mensagens
     * @param response
     * @param err
     * @returns {*|ServerResponse}
     */
    this.messageProcess = function (response, status) {
        response.status(status.CODE).json(this.getLogMessage(status));
        return status.DATA;
    };

    /**
     * @desc Processa log de mensagens
     * @param response
     * @param err
     * @returns {*|ServerResponse}
     */
    this.messageProcessRender = function (response, status, view) {
        response.status(status.CODE).render(view, this.getLogMessage(status));
        return status.DATA;
    };

    /**
     * @desc Recupera mensagem informada
     * @param info
     * @returns {* mensagem concatenada }
     */
    this.getLogMessage = function (status) {
        status.TYPE === "error"
            ? console.error(status.CODE + " - " + status.MESSAGE + " [" + status.ADDITIONAL + "] \n")
            : console.info(status.CODE + " - " + status.MESSAGE + " [" + status.ADDITIONAL + "] DATA:" + JSON.stringify(status.DATA) + "\n");
        this.status = status;
        return status;
    };

    /**
     * @description Acesso ao DM message
     * @returns {Function}
     */
    this.getDmMessage = function () {
        return DmMessage;
    };
};

/**
 * @description Define mensagens
 * @param TYPE
 * @param CODE
 * @param MESSAGE
 * @param DATA
 * @constructor
 */
var DmMessage = function DmMessage(TYPE, CODE, MESSAGE) {
    this.TYPE = TYPE;
    this.CODE = CODE;
    this.MESSAGE = MESSAGE;
    this.TIMESTAMP = new Date();
    this.DATA = '';
    this.ADDITIONAL = ''
};
DmMessage.values = [];
DmMessage.REQUIRED = new DmMessage('error', 500, 'Informar Campos Obrigatórios!');
DmMessage.LOGINERROR = new DmMessage('error', 500, 'Usuário ou senha estão incorretos!');
DmMessage.ACCOUNTERROR = new DmMessage('error', 500, 'Erro ao criar nova conta!');
DmMessage.HANDLEERROR = new DmMessage('error', 500, 'Falha na conexão, tente novamente!');
DmMessage.REMOVEERROR = new DmMessage('error', 500, 'Erro ao remover registro, tente novamente!');
DmMessage.CHANGEERROR = new DmMessage('error', 500, 'Erro ao editar registro, tente novamente!');
DmMessage.CREATEERROR = new DmMessage('error', 500, 'Erro ao criar novo registro!');
DmMessage.CHANGEDPASS = new DmMessage('success', 200, 'Senha alterada com sucesso!');
DmMessage.CHANGED = new DmMessage('success', 200, 'Dados alterados com sucesso!');
DmMessage.CREATED = new DmMessage('success', 201, 'Novo registro cadastrado!');
DmMessage.GETTED = new DmMessage('success', 200, 'Busca sucedida!');
DmMessage.REMOVED = new DmMessage('success', 202, 'Registro removido!');

module.exports = LogsManager;
