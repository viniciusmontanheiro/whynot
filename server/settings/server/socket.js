/**
 * Created by vinicius on 08/04/15.
 */

function Socket(io){
    var server = null;
    io.on('connection', function (socket) {
        console.info("Cliente conectado!!");

        socket.on("ola",function(data){
           console.log(data.mensagem);
        });


        server = socket;
    });

    //Disponibilizando o socket na aplicação
    global.server = server;
};

module.exports = Socket;