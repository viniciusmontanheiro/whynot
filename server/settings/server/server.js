'use strict';

/**
 * @description Configurações do servidor
 * @author Vinícius Montanheiro
 */

var http = require('http');
var path = require('path');
var lodash = require('lodash');
var express = require('express');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var multer = require('multer');
var passport = require('passport');
var session = require('express-session');
var helmet = require('helmet');
var Routes = require('./../../app/routes.js');
var fs = require('fs');
var socketio = require('socket.io');
var Socket = require('./socket');
var Auth = require('../oAuth/passport.js');

function Server() {

    var root = path.normalize(__dirname + '/../../..');
    var app = express();

    //Configurações do express
    app.set("port", 3000);
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(methodOverride());
    app.use(multer());
    app.set('views', root + '/client/app');
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');
    app.use(express.static(path.join(root, '/client')));
    app.use(helmet());
    app.use(session({
        secret: 'teqdegsxzy-www checz',
        resave: true,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());

    //Configurações de segurança
    app.use(helmet.xframe()); // Nenhum iframe pode referênciar essa aplicação
    app.use(helmet.xssFilter()); // Proteção contra ataques xss
    app.use(helmet.nosniff()); // Não permite que o browser modifique o MIME TYPE
    app.disable('x-powered-by'); // Removendo informações da tecnologia usada
    app.use(helmet.hidePoweredBy({setTo: 'PHP 5.5'})); // Atribui informações falsas ao powered by

    //Rotas
    Routes(app, passport);
    //oAuth
    Auth(passport);

    // Estabelecendo servidor
    var server = http.createServer(app);
    // Configurações do socket
    var io = socketio(server);

    // Iniciando servidor
    server.listen(app.get("port"), function (err) {
        if (err) {
            console.error(new Error(':-( Server DOWN!!'), err);
        }
        console.info('Servidor iniciado na porta %d', app.get("port"));
        //Instanciando socket
        Socket(io);
    });
};

module.exports = Server;









