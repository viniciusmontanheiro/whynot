'use strict';

var mongoose = require('mongoose');
var MongoDao = require('../../commons/dao/mongoDao.js');

/**
 * @description Gerênciador de conexões com Mongo
 * @param config
 * @returns {{connection: string, dao: string, connected: boolean}}
 * @constructor
 */
function MongoConnector(config) {

    var status = {
        connection: '',
        dao: new MongoDao(),
        connected: false,
        uri : "mongodb://" + config.host + ":" + config.port + "/" + config.name
    };

    console.info("Iniciando conexão com mongo...");

    mongoose.connect(status.uri, {server: {poolSize: 5}});

    mongoose.connection.on("connected", function () {
        console.info("Mongo connectado em " + config.name);
        status.connection = mongoose;
        status.connected = true;
    });

    mongoose.connection.on("error", function (err) {
        console.error(new Error('Tentativa de conexão com mongo falhou!'), err);
    });

    process.on("SIGINT", function () {
        mongoose.connection.close(function () {
            console.info("Moongose! Desconnectado pelo término da aplicação.");
            process.exit(0);
        });
    });

    return status;
};
exports.connector = MongoConnector;





