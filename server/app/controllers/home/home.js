'use strict';

/**
 * @description Implementação do index controller
 */

exports.init = function (req, res, next) {
    res.render('index',{
        title : "Why Not"
    });
    next();
};


