'use strict';

/**
 * @desc Coleção de produtos
 * @since 02/12/2014
 * @returns product model
 */
var mongoose = require('mongoose');
var productSchema = mongoose.Schema({
    ativo : Boolean,
    destaque : Boolean,
    titulo: String,
    descricao: String,
    quantidade: Number,
    valor: String
});
module.exports = mongoose.model('products', productSchema);
